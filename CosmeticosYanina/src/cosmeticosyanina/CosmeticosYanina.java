package cosmeticosyanina;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class CosmeticosYanina {

    public static void main(String[] args) {
        //categorias y ventas
        
        //clientes
        Clientes miCliente1 = new Clientes();
        miCliente1.nombre = "Yanina";
        miCliente1.direccion = "Av Corrientes 1000";
        miCliente1.metodoPago = "tarjeta";
        miCliente1.mail = "yaina@gmail.com";
        
        Clientes miCliente2 = new Clientes();
        miCliente2.nombre = "Ariel";
        miCliente2.direccion = "Dorrego 456";
        miCliente2.metodoPago = "efectivo";
        miCliente2.mail = "ariel@gmail.com";
        System.out.println(miCliente1);
        //productos
        Productos miproducto = new Productos();
        miproducto.nombre = "lapiz labial";
        miproducto.precio = 300;
        miproducto.descripcion = "rojo cereza";
        
        Productos miproducto2 = new Productos();
        miproducto2.nombre = "base";
        miproducto2.precio = 500;
        miproducto2.descripcion = "base para maquillaje";
        
        Productos miproducto3 = new Productos();
        miproducto3.nombre = "fijador";
        miproducto3.precio = 600;
        miproducto3.descripcion = "";
        
        Productos miproducto4 = new Productos();
        miproducto4.nombre = "delineador";
        miproducto4.precio = 700;
        miproducto4.descripcion = "delineador fino";
        System.out.println(miproducto2);
        
        //venta
        Ventas miVenta1 = new Ventas();
        miVenta1.miCliente = miCliente1;
        miVenta1.fecha = "15/05/2020";
        //miVenta1.listadoDeProductos.add();
        VentaDetalle detalle1 = new VentaDetalle();
        //inicio Proceso interno del programa
        detalle1.miProducto = miproducto2;
        VentaDetalle detalle2 = new VentaDetalle();        
        detalle2.miProducto = miproducto4;
        VentaDetalle detalle3 = new VentaDetalle();
        detalle3.miProducto = miproducto3;
        //fin Proceso interno del programa
        miVenta1.listadoDeProductos.add(detalle1);
        miVenta1.listadoDeProductos.add(detalle2);
        miVenta1.listadoDeProductos.add(detalle3);
        
        
        
//        Ventas miVenta2 = new Ventas();
//        miVenta2.miCliente = miCliente2;
//        miVenta2.fecha = "15/05/2020";
        
        //Nos conectamos a la base de datos
        //1. Conectarnos
        String usuario = "cine";
        String clave = "cine";
        String url = "jdbc:mysql://localhost:3306/cosmeticos_yanina";
        String consultaSql = "SELECT * FROM `productos`";
        Connection miConexion = null;
        
        try {
            miConexion = DriverManager.getConnection(url, usuario, clave);
            PreparedStatement miPreparacion = miConexion.prepareStatement(consultaSql);
            ResultSet miResultado = miPreparacion.executeQuery();
            while (miResultado.next()) {                
                System.out.println("Nombre: " + miResultado.getString("nombre") + " | Precio: " + miResultado.getString("precio"));
            }
            
        } catch (SQLException miExepcion) {
            System.out.println("---Alerta---");
            System.out.println(miExepcion);
        }finally{
            try {
                if (miConexion != null){
                    miConexion.close();
                }
                
            } catch (SQLException ex) {
                System.out.println("Problemas al cerrar la conexion con la base de datos");
                System.out.println(ex);
            }
        }



        System.out.println(miVenta1);
        
    }
    
}
